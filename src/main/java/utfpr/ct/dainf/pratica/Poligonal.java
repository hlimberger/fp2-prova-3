package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();
    
    
    public int getN(){
        
        Iterator<T> it = vertices.iterator();
        
        int numero = 0;
        
        while(it.hasNext()){
            numero = numero + 1;
        }
        
        return numero;
    }
    
    public T get(int i) throws Exception{
        
        int max = vertices.size();
        
        if (i < 0 || i > max ) {
            throw new Exception(String.format("get(%d): índice inválido",i));
        }
        return vertices.get(i);
    }

    public void set(int i, T p) throws Exception{
        int max = vertices.size();
        
        if (i < 0 || i > max ) {
            throw new Exception(String.format("set(%d): índice inválido",i));
        }
        vertices.add(i, p);
    }
    
    public double getComprimento(){
        
        double soma = 0;
        int max = vertices.size();
        
        Ponto p1, p2;
        
        p1 = vertices.get(0);
        
        for(int i = 1; i < max;i++){
            p2 = vertices.get(i);
            soma += p1.dist(p2);
            p1 = vertices.get(i);
        }
       
        
        return soma;
    }
}


