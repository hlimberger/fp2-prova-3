/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

import java.util.Iterator;

/**
 *
 * @author henrique
 */
public class PoligonalFechada extends Poligonal {
    
    
    public double getComprimento(){
        double soma = super.getComprimento();
        
        try{
            Ponto2D pi = super.get(0);
            Ponto2D pf = super.get(1);
            
            soma += pf.dist(pi);
            
        } catch (Exception e){
            System.out.println(e);
        }
        return soma;
    }
}
