/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.pratica.*;
import java.text.DecimalFormat;

public class Pratica {

    public static void main(String[] args) {
        
        PoligonalFechada pf = new PoligonalFechada();
        PontoXZ p1 = new PontoXZ(-3,2);
        PontoXZ p2 = new PontoXZ(-3,6);
        PontoXZ p3 = new PontoXZ(0,2);
        try {
            pf.set(0, p1);
            pf.set(1, p2);
            pf.set(2, p3);
            
            double comp = pf.getComprimento();

            System.out.println("Comprimento da poligonal: " + comp);
        } catch (Exception e){
            System.out.println(e);
        }
    }
    
}
